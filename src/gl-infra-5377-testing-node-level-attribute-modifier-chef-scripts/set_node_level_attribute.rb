#! /usr/bin/env ruby
# frozen_string_literal: false

# For: https://gitlab.com/gitlab-com/gl-infra/production/-/issues/5377

new_node_attribute_name = 'test_section'
new_node_attribute_section = {
  test_1: true,
  test_2: false
}

target_node_name = ENV['TARGET_NODE']
dry_run = (ENV['DRY_RUN'] != 'false' && ENV['DRY_RUN'] != 'no')
unless target_node_name.nil? || target_node_name.empty?
  nodes.find(name: target_node_name) do |node|
    if dry_run
      puts "[Dry-run] Would have added '#{new_node_attribute_name}' to normal attributes of node #{node.name}"
    else
      puts "Adding '#{new_node_attribute_name}' to normal attributes of node #{node.name}"
      new_node_attribute_section.each do |key, value|
        # Using the key-value accessor method will implicitly create a
        # new VividHash node-level attribute sub-section.
        # Reference: https://github.com/chef/chef/blob/2eb1c0ad50fa1680536b8ef78237c188e6cd34ac/lib/chef/node/attribute_collections.rb#L134
        node.normal_attrs[new_node_attribute_name][key] = value
      end
      node.save
    end
  end
end
