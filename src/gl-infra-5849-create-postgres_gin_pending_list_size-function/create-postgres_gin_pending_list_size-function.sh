#! /usr/bin/env bash

set -euo pipefail

set -x

dry_run="${DRY_RUN:-1}"

cp /tmp/production/src/gl-infra-5849-create-postgres_gin_pending_list_size-function/create-function-postgres_gin_pending_list_size.sql /var/opt/gitlab/patroni/scripts/create-function-postgres_gin_pending_list_size.sql
chown gitlab-psql:gitlab-psql /var/opt/gitlab/patroni/scripts/create-function-postgres_gin_pending_list_size.sql

if [ "${dry_run}" == '0' ]; then
    echo 'Creating function:'
    gitlab-psql --file=/var/opt/gitlab/patroni/scripts/create-function-postgres_gin_pending_list_size.sql
    rm /var/opt/gitlab/patroni/scripts/create-function-postgres_gin_pending_list_size.sql
else
    echo "[Dry-run] Would have ran command: gitlab-psql --file=/var/opt/gitlab/patroni/scripts/create-function-postgres_gin_pending_list_size.sql"
fi
